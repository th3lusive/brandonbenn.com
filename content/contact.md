---
title: "Contact"
menu: "main"
weight: 2
draft: false
---

Email: me@brandonbenn.xyz

Feel free to send me your message regarding the content of this website.
I normally reply within 48 hours. If you think you have not received a message
from me after a reasonable amount of time, check your “spam” folder.
